<?php
	
    require 'Database.php';

	class Registro{
		function _construct(){
		}

		public static function ObtenerTodosLosCursos(){
			$consultar = "SELECT cursos.id as id, cursos.nombre as nombre, universidades.nombre as univeridad, centros_investigacion.nombre as centros_investigacion FROM cursos INNER JOIN universidades ON universidades.id = cursos.id_universidad INNER JOIN centros_investigacion ON centros_investigacion.id = cursos.id_centro WHERE cursos.STATUS = 1";

			$resultado = Database::getInstance()->getDb()->prepare($consultar);

			$resultado->execute();

			$tabla = $resultado->fetchAll(PDO::FETCH_ASSOC);

			return ($tabla);

		}
            
         public static function ObtenerPosgrado($usuario){
            $consultar = "SELECT posgrados.id as id, posgrados.nombre as nombre, posgrados.escuela as escuela, posgrados.deseo as deseo FROM posgrados WHERE posgrados.id_alumno = ?";
            try{
            $resultado = Database::getInstance()->getDb()->prepare($consultar);

			$resultado->execute(array($usuario));

			$tabla = $resultado->fetch(PDO::FETCH_ASSOC);

			return ($tabla);
            }catch(PDOException $e){
            echo "Ocurrio un Error, Intentelo Mas tarde";
            }
            return false;
            
            }   
        
        
            public static function ActualizarPosgrado($nombre,$escuela,$deseo,$id_alumno){
    
            $consultar = "UPDATE posgrados SET nombre= ? , escuela= ? ,deseo= ? WHERE id_alumno = ?";
            try{
                $resultado = Database::getInstance()->getDb()->prepare($consultar);
                return $resultado->execute(array($nombre,$escuela,$deseo,$id_alumno));
            }catch(PDOException $e){
                return false;
            }
            
            
        }
        
        
        
     
            

        
	}


?>