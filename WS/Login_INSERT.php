<?php
    require 'Login.php';

    if($_SERVER['REQUEST_METHOD']=='POST'){
        $datos = json_decode(file_get_contents("php://input"),true);
        $respuesta = Registro::InsertarNuevoContacto($datos["telefonoFijo"],$datos["telefonoMovil"],$datos["correo"],$datos["correoAlterno"],$datos["id_alumno"]);
        if($respuesta){
            echo json_encode(array('resultado' => 'Actualizado Correctamente'));
        }else{
            echo json_encode(array('resultado' => 'Hubo un Error'));
        }
        
    }

?>
