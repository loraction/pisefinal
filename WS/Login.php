<?php
	
    require 'Database.php';

	class Registro{
		function _construct(){
		}

		public static function ObtenerTodosLosUsuarios(){
			$consultar = "SELECT * FROM usuarios";

			$resultado = Database::getInstance()->getDb()->prepare($consultar);

			$resultado->execute();

			$tabla = $resultado->fetchAll(PDO::FETCH_ASSOC);

			return ($tabla);

		}
            
        public static function ObtenerDatosPorUsuario($usuario){
            $consultar = "SELECT * FROM usuarios WHERE usuario = ?";
            
            try{
            $resultado = Database::getInstance()->getDb()->prepare($consultar);

			$resultado->execute(array($usuario));

			$tabla = $resultado->fetch(PDO::FETCH_ASSOC);

			return ($tabla);
            }catch(PDOException $e){
            echo "Ocurrio un Error, Intentelo Mas tarde";
            }
            return false;
            
        }
        public static function ObtenerInformacionPorUsuario($usuario){
            
            $consultar = "SELECT alumnos.id as id, alumnos.nombre as nombre, alumnos.paterno as paterno, alumnos.materno as materno, alumnos.natalicio as natalicio, alumnos.sexo as sexo, alumnos.STATUS as STATUS, usuarios.usuario as usuario, usuarios.PASSWORD as PASSWORD, usuarios.email as email, usuarios.url_foto as url_foto, carreras.nombre as carrera FROM alumnos INNER JOIN usuarios ON usuarios.id = alumnos.id_usuario INNER JOIN carreras ON carreras.id = alumnos.id_carrera WHERE usuarios.usuario = ? ";
            
            
            /*$consultar = "SELECT usuarios.id as id, infousuarios.id as viajeroID, infousuarios.nombre as nombre, infousuarios.a_paterno as ap, infousuarios.a_materno as am, infousuarios.telefono as telefono, infousuarios.correo as correo, usuarios.usuario as usuario, usuarios.contraseña as contraseña, usuarios.id_perfil as tipoUsuario, infousuarios.edad as edad, infousuarios.informacion as informacion, infousuarios.sexo as sexo, infousuarios.estado as estado FROM infousuarios INNER JOIN usuarios ON infousuarios.id_usuario = usuarios.id WHERE usuarios.usuario = ?";*/
            
            try{
            $resultado = Database::getInstance()->getDb()->prepare($consultar);

			$resultado->execute(array($usuario));

			$tabla = $resultado->fetch(PDO::FETCH_ASSOC);

			return ($tabla);
            }catch(PDOException $e){
            echo "Ocurrio un Error, Intentelo Mas tarde";
            }
            return false;
            
        }
        
        
        public static function ObtenerContactoPorID($id){
            
            $consultar = "SELECT contactos.telefono as telefono, contactos.celular as celular, contactos.email as email, contactos.email_alt as email_alt FROM contactos WHERE contactos.id_alumno = ?";
            
            try{
            $resultado = Database::getInstance()->getDb()->prepare($consultar);

			$resultado->execute(array($id));

			$tabla = $resultado->fetch(PDO::FETCH_ASSOC);

			return ($tabla);
            }catch(PDOException $e){
            echo "Ocurrio un Error, Intentelo Mas tarde";
            }
            return false;
            
        }
        
        
        public static function InsertarNuevoContacto($telefonoFijo,$telefonoMovil,$correo,$correoAlterno,$id_alumno){
            
    
            $consultar = "UPDATE contactos SET telefono= ? , celular= ? ,email= ?,email_alt= ? WHERE id_alumno = ?";
            try{
                $resultado = Database::getInstance()->getDb()->prepare($consultar);
                return $resultado->execute(array($telefonoFijo,$telefonoMovil,$correo,$correoAlterno,$id_alumno));
            }catch(PDOException $e){
                return false;
            }
            
            
        }
        
        
      
	}


?>